package sample;

import javax.xml.crypto.Data;
import java.sql.*;

public class DataBase {
    private Connection connection = null;
    private static DataBase baseConnection = null;

    private DataBase(String u, String p, String n) {
        String url = "jdbc:mysql://localhost:3306/";
        try {
            connection = DriverManager.getConnection(url, u, p);
            System.out.println("Active Connection!");
            try {
                Statement statement = connection.createStatement();
                int sqlQueryResult = statement.executeUpdate("CREATE DATABASE IF NOT EXISTS " + n);
                if (sqlQueryResult != 0) {
                    System.out.println("New Database Created!");
                } else {
                    System.out.println("Database Exist");
                }
                connection = DriverManager.getConnection(url + n, u, p);
            } catch (Exception ex) {
                System.out.println("Error in Statement " + ex.getClass());
            }
        } catch (SQLException sql) {
            System.out.println("Connection Failed!");
        }

    }

    public Connection connect() {
        return connection;
    }

    public static DataBase getConnection(String username, String password, String databaseName) {
        if (baseConnection == null) {
            baseConnection = new DataBase(username, password, databaseName);
        }
        return baseConnection;
    }


    public void makeQuery(Integer id_player, String name_player, Integer act) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT  INTO actions (id, playerid, name, action) values (null, ?, ?, ?)");
            preparedStatement.setInt(1, id_player);
            preparedStatement.setString(2, name_player);
            preparedStatement.setInt(3, act);
            int e = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.out.println("Error In Create Table artist ");
            e.printStackTrace();
        }
    }

}
