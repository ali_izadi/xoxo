package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import org.w3c.dom.Node;

import javax.swing.*;
import java.net.URL;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    Boolean b = false;
    GameHandler handler;
    Map<Integer, Integer> map = new HashMap<>();
    @FXML
    TextField p1, p2;
    String[] names = new String[2];
    @FXML
    GridPane mainGrid;
    @FXML
    Button ptop, ptoc, start;
    @FXML
    Label winner;

    DataBase db;

    private String name1 = null;
    private String name2 = "Agent";
    private Integer startMode = 0;

    @FXML
    public void btnHandler(ActionEvent event) {
        Object node = event.getSource();
        Button btn = (Button)node;

        //by computer
        if (startMode == 0) {
            if (!b) {
                chooser(btn);
            } else {

            }
        }

        //by player
        if (startMode == 1) {
            chooser(btn);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        mainGrid.setDisable(true);
        start.setDisable(true);
        p1.setDisable(true);
        p2.setDisable(true);
        db = DataBase.getConnection("root", "root", "game");

        for (int i = 1; i < 10; i++) {
            map.put(i, null);
        }
    }

    private Integer check() {
        if (map.get(1) == map.get(2) && map.get(2) == map.get(3)) {
            return map.get(1);
        }
        if (map.get(4) == map.get(5) && map.get(5) == map.get(6)) {
            return map.get(4);
        }
        if (map.get(7) == map.get(8) && map.get(7) == map.get(8)) {
            return map.get(7);
        }
        if (map.get(1) == map.get(4) && map.get(4) == map.get(7)) {
            return map.get(1);
        }
        if (map.get(2) == map.get(5) && map.get(5) == map.get(8)) {
            return map.get(2);
        }
        if (map.get(3) == map.get(6) && map.get(6) == map.get(9)) {
            return map.get(3);
        }
        if (map.get(1) == map.get(5) && map.get(5) == map.get(9)) {
            return map.get(1);
        }
        if (map.get(3) == map.get(5) && map.get(5) == map.get(7)) {
            return map.get(3);
        }
        return null;
    }

    public void startGame(ActionEvent actionEvent) {
        if (startMode == 1) {
            name1 = p1.getText();
            name2 = p2.getText();
            if (!name1.isEmpty() && !name2.isEmpty()) {
                mainGrid.setDisable(false);
                start.setDisable(true);
                names[0] = name1;
                names[1] = name2;
                System.out.println(names[0] + "   " + names[1]);
                handler = new GameHandler();
            }
        } else {
            name1 = p1.getText();
            if (!name1.isEmpty()) {
                names[0] = name1;
                names[1] = name2;
                mainGrid.setDisable(false);
                start.setDisable(true);
                System.out.println(names[0] + "   " + names[1]);
                handler = new GameHandler();
            }
        }
    }

    public void chooseMode(ActionEvent event) {
        Object node = event.getSource();
        Button btn = (Button)node;
        if (btn.getId().equals("ptop")) {
            startMode = 1;
            p1.setDisable(false);
            p2.setDisable(false);
            start.setDisable(false);
            ptoc.setDisable(true);
            ptop.setDisable(true);
        } else {
            startMode = 0;
            p1.setDisable(false);
            start.setDisable(false);
            ptop.setDisable(true);
            ptoc.setDisable(true);
        }
    }

    private void chooser(Button btn) {
        if (b) {
            b = !b;
            Integer tableNumber = Integer.valueOf(btn.getId().replace("bt", ""));
            map.put(tableNumber, 1);
            btn.setText("X");//prints out Click Me
            btn.setStyle("-fx-background-color: #E79CA9");
            System.out.println(map);
            if (check() != null) {
                winner.setText(names[check()]);
                mainGrid.setDisable(true);
            }
            db.makeQuery(1, names[1], tableNumber);
        } else {
            b = !b;
            Integer tableNumber = Integer.valueOf(btn.getId().replace("bt", ""));
            map.put(tableNumber, 0);
            btn.setText("O");
            System.out.println(map);
            System.out.println(check());
            if (check() != null) {
                winner.setText(names[check()]);
                mainGrid.setDisable(true);
            }
            db.makeQuery(0, names[0], tableNumber);
        }
    }
}
