package sample2;

import java.util.Map;

public class BoardModel {
    private Map<Integer, Integer> board;

    BoardModel(Map<Integer, Integer> b) {
        this.board = b;
        for (int i = 1; i < 10; i++) {
            this.board.put(i, null);
        }
    }

    public Map<Integer, Integer> getBoard() {
        return this.board;
    }

}
